package fs

import (
	"bytes"
	"encoding/gob"
	"io"
	"io/ioutil"
	"os"
	"sync"

	"quicoin/core/database"
	safebytes "quicoin/core/utils/bytes"
)

var (
	// defaultSize for the store
	defaultSize = 1 << 10
)

// DB represents a file backed db implementing the Database interface
type DB struct {
	mu       sync.RWMutex
	store    map[string][]byte
	filename string
}

// New returns a new instance of DB
func New(filename string) (*DB, error) {
	db := &DB{
		store:    make(map[string][]byte, defaultSize),
		filename: filename,
	}
	// create db file
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
		if err != nil {
			return nil, err
		}
		f.Close()
	} else {
		err := db.seedStoreMap()
		if err != nil {
			return nil, err
		}
	}
	return db, nil
}

// Has implements the Database interface
func (db *DB) Has(key []byte) (bool, error) {
	db.mu.RLock()
	defer db.mu.RUnlock()

	if db.store == nil {
		return false, database.ErrClosed
	}
	_, ok := db.store[string(key)]
	return ok, nil
}

// Get implements the Database interface
func (db *DB) Get(key []byte) ([]byte, error) {
	db.mu.Lock()
	defer db.mu.Unlock()

	if db.store == nil {
		return nil, database.ErrClosed
	}

	if item, ok := db.store[string(key)]; ok {
		return safebytes.CopyBytes(item), nil
	}
	return nil, database.ErrNotFound
}

// Put implements the Database interface
func (db *DB) Put(key, value []byte) error {
	db.mu.Lock()
	defer db.mu.Unlock()

	if db.store == nil {
		return database.ErrClosed
	}

	db.store[string(key)] = safebytes.CopyBytes(value)
	return db.write()
}

// Update implements the Database interface
func (db *DB) Update(key, value []byte) error {
	return nil
}

// Delete implements the Database interface
func (db *DB) Delete(key []byte) error {
	return nil
}

// write saves the content of the store map to disk
func (db *DB) write() error {
	f, err := os.OpenFile(db.filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}

	err = gob.NewEncoder(f).Encode(db.store)
	if err != nil {
		return err
	}

	f.Close()

	return nil
}

// seedStoreMap parses the file and saves content to memory
func (db *DB) seedStoreMap() error {
	db.mu.Lock()
	defer db.mu.Unlock()

	var data map[string][]byte
	b, err := ioutil.ReadFile(db.filename)
	if err != nil {
		return err
	}
	err = gob.NewDecoder(bytes.NewBuffer(b)).Decode(&data)
	if err != nil && err != io.EOF {
		return err
	}

	db.store = data
	return nil
}
