package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"quicoin/core/node"
)

func generateTransID() string {

	maxrange := 777777
	minrange := 10000 - 666
	randnum := rand.Intn(maxrange-minrange) + maxrange
	randstring := StringWithCharset(24)
	idname := strconv.Itoa(randnum) + randstring
	return idname
}

// DecodeReq ... This helps decode a json request body into an interface
func DecodeReq(r *http.Request, model interface{}) error {
	defer r.Body.Close()
	b, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(b, model)
	r.Body = ioutil.NopCloser(bytes.NewBuffer(b))
	if err != nil {
		fmt.Printf("%+v\n", err.Error())
		return err
	}
	return err
}

// StringWithCharset ...
func StringWithCharset(length int) string {
	charset := "JESUSiskingskingdomMoneyALPHAaandOMEGAsLordOFLordssLOVEECONOMY"
	var seededRand *rand.Rand = rand.New(
		rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// Contains check for i in s slice ([]string)
func Contains(s []string, i string) bool {
	for _, a := range s {
		if a == i {
			return true
		}
	}
	return false
}

// ConnAddr returns the connection address to use in network communicating
// connAddr can be any of the following:
// http://localhost:port or http://ip:port
func ConnAddr(c *node.Config) string {
	var addr string
	addr = fmt.Sprintf("%s:%s", c.HTTPHost, c.HTTPPort)

	if c.NodeType != "local" && c.IP != "" {
		addr = c.IP
	}

	return addr
}

//MakeRequest ...
func MakeRequest(buildID string, method, endpoint string, body io.Reader) (*http.Response, error) {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest(method, endpoint, body)
	if err != nil {
		return nil, err
	}
	req.Close = true
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-LID-BUILD", buildID)
	req.Header.Add("X-LID-Outbound", "broadcast")
	return client.Do(req)
}

// MakeMultipartRequest ...
func MakeMultipartRequest(buildID string, method, endpoint string, body io.Reader) (*http.Response, error) {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest(method, endpoint, body)
	if err != nil {
		return nil, err
	}
	req.Close = true
	req.Header.Add("Content-Type", "binary/octet-stream")
	req.Header.Add("X-LID-BUILD", buildID)
	req.Header.Add("X-LID-Outbound", "broadcast")
	return client.Do(req)
}
