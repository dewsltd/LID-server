package janitor

import (
	"bytes"
	"encoding/gob"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"time"

	"quicoin/core/blockchain"
)

// JN is an instance of a janitor
type JN struct {
	config *Config
	stop   chan bool
}

// Config represents a janitor config
type Config struct {
	DispatchInterval time.Duration
	DirPath          string
	MaxBlocksDel     int
}

// New creates a new Janitor instance
func New(config *Config) *JN {
	return &JN{
		config: config,
		stop:   make(chan bool),
	}
}

//Dispatch starts the janitor process
func (j *JN) Dispatch() {
	log.Println("[JN]: Janitor instance dispatched")
	tx := time.NewTicker(j.config.DispatchInterval)
	for {
		select {
		case <-tx.C:
			j.handleDispatch()
		case <-j.stop:
			tx.Stop()
			return
		}
	}
}

func (j *JN) handleDispatch() {
	dirs, err := ioutil.ReadDir(j.config.DirPath)
	if err != nil {
		log.Printf("[JN]: failed to read dir path: %v", err)
		return
	}
	sort.Slice(dirs, func(i, j int) bool {
		return dirs[i].ModTime().Unix() < dirs[j].ModTime().Unix()
	})

	// Walk directory and remove n blocks

	for _, dir := range dirs {
		go func(d os.FileInfo) {
			var chain []blockchain.Block
			chainPath := filepath.Join(j.config.DirPath, dir.Name(), "chain.bin")
			b, err := ioutil.ReadFile(chainPath)
			if err != nil {
				log.Printf("[JN]: failed to read wallet path, err %v", err)
				return
			}
			dec := gob.NewDecoder(bytes.NewBuffer(b))
			err = dec.Decode(&chain)
			if err != nil {
				log.Printf("[JN]: failed to decode wallet, err %v", err)
				return
			}
			err = j.clearOldBlocks(chain, chainPath)
			if err != nil {
				log.Printf("[JN]: failed to check/clear old blocks, err %v", err)
				return
			}

		}(dir)
	}
}

// clearOldBlocks deletes n(MaxBlocksDel) blocks from a chain, and returns the
// modified chain
func (j *JN) clearOldBlocks(blocks []blockchain.Block, filePath string) error {
	blocksToDel := len(blocks) - j.config.MaxBlocksDel
	if blocksToDel < 1 {
		return nil
	}
	newChain := blocks[blocksToDel:]

	f, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	err = f.Truncate(0)
	if err != nil {
		return err
	}

	// write new chain state to disk
	var buf bytes.Buffer
	err = gob.NewEncoder(&buf).Encode(newChain)
	if err != nil {
		return err
	}
	// save to disk
	err = ioutil.WriteFile(filePath, buf.Bytes(), 0700)
	if err != nil {
		return err
	}

	return nil
}

// Stop sends a stop signal the janitor instance
func (j *JN) Stop() {
	go func() {
		j.stop <- true
	}()
}
