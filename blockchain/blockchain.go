package blockchain

import (
	"quicoin/core/utils/logging"
)

// Handler represents the blockchain handlers
type Handler struct {
	//config *node.Config
	log logging.Logger

	// Indicates the node ip
	nodeIP string
}

// NewHandler returns a new blockchain handler
func NewHandler(logFactory logging.Factory, nodeip string) (*Handler, error) {
	chainLogger, err := logFactory.AddDir("BChain Handler", "", true)
	if err != nil {
		return nil, err
	}
	return &Handler{
		//config: cfg,
		log:    chainLogger,
		nodeIP: nodeip,
	}, nil
}
