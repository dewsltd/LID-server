// Package limitter is a standalone configurable http rate limitting middleware
package limitter

import (
	"net"
	"net/http"
	"sync"
	"time"

	"quicoin/core/utils/logging"

	"golang.org/x/time/rate"
)

// RateLimit represents a configured rate limitter
type RateLimit struct {
	log             logging.Logger
	Sessions        map[string]*Session
	TokenBucketSize int
	CleanupAfter    int

	mu sync.Mutex
}

// Session represents a rate limitter for each active session/hits
type Session struct {
	rlimitter   *rate.Limiter
	activeSince time.Time
}

// New Initializes a rate limitter
func New(log logging.Logger, bucketSize int, cleanupAfter int) *RateLimit {
	sessions := make(map[string]*Session)
	return &RateLimit{
		log:             log,
		Sessions:        sessions,
		TokenBucketSize: bucketSize,
		CleanupAfter:    cleanupAfter,
	}
}

// Limit is a http handler to check, register and rate limit new sessions/hits
func (rl *RateLimit) Limit(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			rl.log.Error("error retrieving request ip: %v", err)
			http.Error(w, "Server Error", http.StatusInternalServerError)
			return
		}
		rlimiter := rl.getSession(ip)
		if rlimiter.Allow() == false {
			http.Error(w, http.StatusText(429), http.StatusTooManyRequests)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// getSession retrieves or register a new rate limit hit
func (rl *RateLimit) getSession(ip string) *rate.Limiter {
	rl.mu.Lock()
	defer rl.mu.Unlock()

	session, ok := rl.Sessions[ip]
	if !ok {
		// accept N requests per second
		rt := rate.Every(1 * time.Second / time.Duration(rl.TokenBucketSize))
		rlimitter := rate.NewLimiter(rt, 3)
		rl.Sessions[ip] = &Session{rlimitter, time.Now()}
		return rlimitter
	}

	session.activeSince = time.Now()
	return session.rlimitter
}

// Cleanup removes sessions not seen for the past n minutes
func (rl *RateLimit) Cleanup() {
	for {
		time.Sleep(time.Minute)

		rl.mu.Lock()
		for ip, session := range rl.Sessions {
			if time.Since(session.activeSince) > time.Duration(rl.CleanupAfter)*time.Minute {
				delete(rl.Sessions, ip)
			}
		}

		rl.mu.Unlock()
	}
}
