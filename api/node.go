package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"sync"
	"time"

	"quicoin/core/models"
	"quicoin/core/utils"
	safebytes "quicoin/core/utils/bytes"
)

// AcceptPeerGossip accepts a gossip from a connecting node
func (s *Server) AcceptPeerGossip(w http.ResponseWriter, r *http.Request) {
	var req models.BootstrapReq
	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	peers := s.chain.GetServers()
	if utils.Contains(peers, req.IP) {
		// requested peer already exist, send peer list
		utils.RespondWithJSON(w, http.StatusCreated, map[string]interface{}{
			"status": "success",
			"peers":  peers,
		})
		return
	}
	// add peer to node peer list
	peers = append(peers, req.IP)

	f, err := os.OpenFile(peerListFile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		s.log.Error("failed to open peer list, err: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Error updating peer list")
		return
	}

	if err := json.NewEncoder(f).Encode(peers); err != nil {
		s.log.Error("failed to encode peer list, err: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Error updating peer list")
		return
	}
	// close file
	if err := f.Close(); err != nil {
		s.log.Fatal("failed to close peerListFile: %+v", err)
		return
	}

	// gossip new peer to others
	go s.gossipPeer(req, peers)

	utils.RespondWithJSON(w, http.StatusCreated, map[string]interface{}{
		"status": "success",
		"peers":  peers,
	})
}

// AcceptPeerSync receives a node sync request and currates a list of wallets
// present on its node and dispatches it to the calling node.
func (s *Server) AcceptPeerSync(w http.ResponseWriter, r *http.Request) {
	var req models.SyncRequest
	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	// confirm current node hasn't already accepted this request
	current := utils.ConnAddr(s.config)
	if req.Origin == current {
		utils.RespondWithOk(w, "Skipping sync request")
		return
	}

	if utils.Contains(req.AcceptedNodes, current) {
		utils.RespondWithOk(w, "Sync Request already accepted")
		return
	}

	go func() {
		err := s.streamBlocksToSyncOrigin(req, current)
		if err != nil {
			s.log.Error("failed to stream sync archive: %v", err)
			utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
			return
		}
	}()

	utils.RespondWithOk(w, "Accepted sync request")
}

// ProcessSync processes the received wallet blocks summary from a peer
func (s *Server) ProcessSync(w http.ResponseWriter, r *http.Request) {
	// save uploaded file to disk
	temp := fmt.Sprintf("%s.tar.gz", utils.StringWithCharset(10))
	archiveFile, err := os.Create(temp)
	if err != nil {
		s.log.Error("failed to create archive temp file: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}
	_, err = io.Copy(archiveFile, r.Body)
	if err != nil {
		s.log.Error("failed to copy archive file from request: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	// create temp dir
	err = os.Mkdir("archive_temp", 0755)
	if err != nil {
		s.log.Error("failed to create archive_temp: %v", err)
		return
	}

	_, err = exec.Command("tar", "-xzf", temp, "-C", "archive_temp").Output()
	if err != nil {
		s.log.Error("failed to unzip archive: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	// walk archive and move to current node's chain filesystem
	pwd, err := os.Getwd()
	if err != nil {
		s.log.Error("failed to retrieve pwd: %v", err)
		return
	}

	dirname := fmt.Sprintf("%s/archive_temp/data/", pwd)
	chains, err := ioutil.ReadDir(dirname)
	if err != nil {
		s.log.Error("failed to read archive_temp dir: %v", err)
		return
	}

	// mv price and tx data
	if err := os.Rename(pwd+"/archive_temp/price.fdb", pwd+"/price.fdb"); err != nil {
		s.log.Error("failed to extract price fdb: %v", err)
		return
	}

	if err := os.Rename(pwd+"/archive_temp/transactions.fdb", pwd+"/transactions.fdb"); err != nil {
		s.log.Error("failed to extract transactions fdb: %v", err)
		return
	}

	var wg sync.WaitGroup
	wg.Add(len(chains))
	c := make(chan error)

	go func() {
		wg.Wait()
		close(c)
	}()

	blocksCount := len(chains)

	for _, ch := range chains {
		go func(info os.FileInfo, bc int) {
			defer wg.Done()

			if !info.IsDir() {
				return
			}

			// confirm chain data already exist, replace content
			chainDir := pwd + "/data/" + info.Name()
			if _, err := os.Stat(chainDir); !os.IsNotExist(err) {
				// remove directory
				err := os.RemoveAll(chainDir)
				if err != nil {
					bc--
					c <- fmt.Errorf("failed to remove existing chain dir: %v", err)
					return
				}
			}
			// move to node's data directory
			dirToMv := dirname + info.Name()
			out, err := exec.Command("mv", dirToMv, pwd+"/data/").CombinedOutput()
			if err != nil {
				bc--
				c <- fmt.Errorf("failed to move archive data: %v, out: %v", err, string(out))
				return
			}
			return
		}(ch, blocksCount)
	}

	for err := range c {
		if err != nil {
			s.log.Error("process sync err: %v", err)
			return
		}
	}

	// cleanup
	err = os.RemoveAll("archive_temp")
	if err != nil {
		s.log.Error("failed to remove archive_temp dir: %v", err)
		return
	}

	err = os.Remove(temp)
	if err != nil {
		s.log.Error("failed to remove archive: %v", err)
		return
	}

	s.log.Info("Synced %d blocks", blocksCount)

	utils.RespondWithOk(w, "Processed sync blocks")
	return
}

// ArchiveBlocks compresses blocks from the `/data` directory and returns an
// archive file
func (s *Server) ArchiveBlocks(w http.ResponseWriter, r *http.Request) {
	archiveFile := fmt.Sprintf("%s.tar.gz", utils.StringWithCharset(10))
	_, err := exec.Command("tar", "-czf", archiveFile, "data").Output()
	if err != nil {
		s.log.Error("failed to archive blocks: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve blocks at this time")
		return
	}
	file, err := os.Open(archiveFile)
	if err != nil {
		s.log.Error("failed to open chain archive: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve blocks at this time")
		return
	}
	defer file.Close()

	fst, err := file.Stat()
	if err != nil {
		s.log.Error("failed to stat chain archive: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve blocks at this time")
		return
	}

	// prepare header props
	contentLength := strconv.FormatInt(fst.Size(), 10)
	fname := "network.tar.gz"
	w.Header().Set("Content-Disposition", "attachment; filename=\""+fname+"\"")
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	w.Header().Set("Content-Length", contentLength)

	// copy file to download stream
	f, err := io.Copy(w, file)
	if err != nil {
		s.log.Error("failed to send archive: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve blocks at this time")
		return
	}

	// confirm file content was completely streamed to client
	if f < fst.Size() {
		s.log.Error("failed to stream archive: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve blocks at this time")
		return
	}

	// cleanup
	err = os.Remove(archiveFile)
	if err != nil {
		s.log.Error("failed to remove archive file: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve blocks at this time")
		return
	}

	return
}

// GetPrice returns the current price configured on a Node
func (s *Server) GetPrice(w http.ResponseWriter, r *http.Request) {
	var pricingResp models.PriceInfo
	priceData, err := s.priceDB.Get([]byte("price"))
	if err != nil {
		s.log.Error("failed to retrive price data: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve price at this time")
		return
	}
	pricingResp.CurrentPrice = safebytes.Float64frombytes(priceData)

	utils.RespondWithJSON(w, http.StatusOK, map[string]interface{}{
		"status": "success",
		"data":   pricingResp,
	})
}

// GetPriceHistory returns the price change series
func (s *Server) GetPriceHistory(w http.ResponseWriter, r *http.Request) {
	var data models.PriceSeriesData
	series, err := s.priceDB.Get([]byte("price_series"))
	if err != nil {
		s.log.Error("failed to retrive price series data: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve price history at this time")
		return
	}
	if err := safebytes.UnmarshalBytes(series, &data); err != nil {
		s.log.Error("failed to unmarshal price series data: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve price history at this time")
		return
	}

	st := strconv.FormatFloat(data.PriceChange, 'f', 10, 64)
	utils.RespondWithJSON(w, http.StatusOK, map[string]interface{}{
		"status":       "success",
		"data":         data.Data,
		"price_change": st,
		"timestamp":    time.Now().Format(time.RFC3339),
	})
}

// streamBlocksToSyncOrigin archives the state of the blockchain on a node and
// sends archive over the wire to sync request origin
func (s *Server) streamBlocksToSyncOrigin(req models.SyncRequest, currentNode string) error {
	// generate archive
	archiveFile := fmt.Sprintf("%s.tar.gz", utils.StringWithCharset(10))
	_, err := exec.Command("tar", "-czf", archiveFile, "data", "price.fdb", "transactions.fdb").Output()
	if err != nil {
		return err
	}

	// package archive and upload to sync origin
	file, err := os.Open(archiveFile)
	if err != nil {
		return err
	}
	defer file.Close()

	// upload to origin
	origin := fmt.Sprintf("http://%s/node/sync-r", req.Origin)
	resp, err := utils.MakeMultipartRequest(s.config.BuildHash, "POST", origin, file)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// cleanup
	err = os.Remove(archiveFile)
	if err != nil {
		return err
	}

	return nil
}

// Deprecated: stopped support for multiple peer syncing
// dispatchSyncReqToPeers dispatches a node sync request to the current node
// peers
func (s *Server) dispatchSyncReqToPeers(req models.SyncRequest) error {
	// TODO: update when ip implementation is up
	ownPeer := utils.ConnAddr(s.config)
	peersToSendTo := make(map[string]bool)
	peers := s.chain.GetServers()
	for _, peer := range peers {
		peersToSendTo[peer] = true
	}
	// extract peers not found in request AcceptedNodes
	for peer := range peersToSendTo {
		if utils.Contains(req.AcceptedNodes, peer) || peer == ownPeer || peer == req.Origin {
			delete(peersToSendTo, peer)
		}
	}

	// dispatch request to peers
	var wg sync.WaitGroup
	wg.Add(len(peersToSendTo))
	c := make(chan error)

	go func() {
		wg.Wait()
		close(c)
	}()

	for p := range peersToSendTo {
		s.log.Info("sending to peer=%v", p)
		go func(peer string) {
			defer wg.Done()
			data, _ := json.Marshal(req)
			b := bytes.NewBuffer(data)
			endpoint := fmt.Sprintf("http://%s/node/sync", peer)
			resp, err := utils.MakeRequest(s.config.BuildHash, "POST", endpoint, b)
			if err != nil {
				c <- err
				return
			}
			resp.Body.Close()
		}(p)
	}

	// read errors
	for err := range c {
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Server) getNodeWallets() (models.SyncPayload, error) {
	var data models.SyncPayload

	dirs, err := ioutil.ReadDir("./data")
	if err != nil {
		return data, err
	}

	for _, dir := range dirs {
		// TODO: update to retrieve data.bin data
		d := models.BlockData{
			WalletAddress: dir.Name(),
		}
		lastBlock, err := s.chain.LastBlock(d.WalletAddress)
		if err != nil {
			return data, err
		}
		d.PrivateKeyHash = lastBlock.PrivateKeyHash
		d.Hash = lastBlock.Hash

		data.Blocks = append(data.Blocks, d)
	}

	return data, nil
}

func (s *Server) gossipPeer(req models.BootstrapReq, modifiedPeers []string) {
	ownPeer := utils.ConnAddr(s.config)

	b, _ := json.Marshal(req)
	peers := modifiedPeers
	if len(peers) < 1 {
		return
	}

	ps := defaultPeerSampling
	for ps > 0 {
		// sample a random peer from list
		ps = ps - 1
		p := s.chain.RandomInt(0, len(peers)-1)
		peer := peers[p]

		if peer == ownPeer || peer == req.IP {
			continue
		}

		s.log.Info("Gossiping to %s", peer)

		endpoint := fmt.Sprintf("http://%s/node/gossip-peer", peer)
		resp, err := utils.MakeRequest(
			s.config.BuildHash,
			"POST",
			endpoint,
			bytes.NewBuffer(b),
		)

		if err != nil {
			s.log.Error("failed to gossip peer [%s], err: %v", peer, err)
			continue
		}

		err = resp.Body.Close()
		if err != nil {
			s.log.Error("failed to close peer gossip response body: %v", err)
			return
		}

		if resp.StatusCode >= 300 {
			return
		}
	}
}
