package logging

import (
	"fmt"
	"os"
	"runtime"
	"strings"
	"time"
)

var (
	// DefaultLogDir for storing logs
	DefaultLogDir = "~/.quicoin/logs"
	filePrefix    = "QUI-server/"
)

// Log represents a logger
type Log struct {
	config Config
}

// Config represents a logger config
type Config struct {
	Directory string
	Prefix    string
}

// New creates a new logger
func New(config Config) (*Log, error) {
	if err := os.MkdirAll(config.Directory, os.ModePerm); err != nil {
		return nil, err
	}
	l := &Log{
		config: config,
	}

	return l, nil
}

// NewFactoryLogger returns a logger with default config
func NewFactoryLogger() (*Log, error) {
	config := Config{
		Directory: DefaultLogDir,
	}
	return New(config)
}

func (l *Log) doLog(level Level, format string, args ...interface{}) {
	output := l.format(level, format, args...)

	fmt.Print(output)
}

func (l *Log) format(level Level, format string, args ...interface{}) string {
	progLoc := "?"
	if _, file, no, ok := runtime.Caller(3); ok {
		progLoc = fmt.Sprintf("%s#%d", file, no)
	}
	if i := strings.Index(progLoc, filePrefix); i != -1 {
		progLoc = progLoc[i+len(filePrefix):]
	}
	text := fmt.Sprintf("%s: %s", progLoc, fmt.Sprintf(format, args...))

	prefix := ""
	if l.config.Prefix != "" {
		prefix = fmt.Sprintf(" <%s>", l.config.Prefix)
	}
	return fmt.Sprintf("%s[%s]%s %s\n",
		level,
		time.Now().Format("01-02-06|15:04:05"),
		prefix,
		text)
}

// Fatal ...
func (l *Log) Fatal(format string, args ...interface{}) { l.doLog(Fatal, format, args...) }

// Error ...
func (l *Log) Error(format string, args ...interface{}) { l.doLog(Error, format, args...) }

// Warn ...
func (l *Log) Warn(format string, args ...interface{}) { l.doLog(Warn, format, args...) }

// Info ...
func (l *Log) Info(format string, args ...interface{}) { l.doLog(Info, format, args...) }

// Debug ...
func (l *Log) Debug(format string, args ...interface{}) { l.doLog(Debug, format, args...) }

// Verbo ...
func (l *Log) Verbo(format string, args ...interface{}) { l.doLog(Verbose, format, args...) }
