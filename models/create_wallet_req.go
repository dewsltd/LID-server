package models

// CreateWallet ...
type CreateWallet struct {
	PrivateKey    string `json:"private_key"`
	WalletName    string `json:"wallet_name"`
	WalletAddress string `json:"wallet_address"`
	IsBroadcasted bool   `json:"is_broadcasted"`
}
