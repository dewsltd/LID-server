package models

// TrxData ...
type TrxData struct {
	IsBroadcasted           bool    `json:"is_broadcasted"`
	SenderBlockID           string  `json:"sender_block_id"`
	RecieverBlockID         string  `json:"reciever_block_id"`
	Amount                  float64 `json:"amount"`
	Sender                  string  `json:"sender"`
	Reciever                string  `json:"reciever"`
	SPrivateKey             string  `json:"s_private_key"`
	RPrivateKey             string  `json:"r_private_key"`
	SenderLastBlockAmount   float64 `json:"sender_last_block_amount"`
	RecieverLastBlockAmount float64 `json:"reciever_last_block_amount"`
}
