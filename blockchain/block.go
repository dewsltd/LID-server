package blockchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"time"

	"quicoin/core/models"
	safebytes "quicoin/core/utils/bytes"
)

// Block represents
type Block struct {
	Amount         float64   `json:"amount"`
	ID             string    `json:"id"`
	Hash           string    `json:"hash"`
	Sender         string    `json:"sender"`
	Reciever       string    `json:"reciever"`
	PrevHash       string    `json:"prev_hash"`
	PrivateKeyHash string    `json:"private_key_hash"`
	Memo           string    `json:"memo"`
	CreatedAt      time.Time `json:"created_at"`
}

var walletDataPath = "./data/"

// SaveBlock saves a block to an address chain
func (h *Handler) SaveBlock(address string, block Block) error {
	return h.saveBlock(address, block)
}

func (h *Handler) saveBlock(address string, block Block) error {
	var (
		err   error
		chain []Block
	)
	walletPath := walletDataPath + address
	_, err = os.Stat(walletPath)
	if err != nil && os.IsNotExist(err) {
		if err := os.MkdirAll(walletPath, 0755); err != nil {
			return err
		}

		chain = append(chain, block)
		chainBytes, err := safebytes.MarshalBytes(chain)
		if err != nil {
			return err
		}
		err = h.WriteFile(walletPath+"/chain.bin", chainBytes, 0700)
		if err != nil {
			return err
		}
		return nil
	}

	// get current chain
	chain, err = h.retrieveChain(address)
	if err != nil {
		return err
	}

	chain = append(chain, block)
	chainBytes, err := safebytes.MarshalBytes(chain)
	if err != nil {
		return err
	}

	err = h.WriteFile(walletPath+"/chain.bin", chainBytes, 0700)
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) retrieveChain(address string) ([]Block, error) {
	walletPath := walletDataPath + address
	_, err := os.Stat(walletPath)
	if err != nil && os.IsNotExist(err) {
		return nil, err
	}
	var chain []Block

	dataByte, err := h.ReadFile(walletPath + "/chain.bin")
	if err != nil {
		return nil, err
	}
	err = safebytes.UnmarshalBytes(dataByte, &chain)
	if err != nil {
		return nil, err
	}

	return chain, nil
}

// PrintChain ...
func (h *Handler) PrintChain(address string) ([]Block, error) {
	chain, err := h.retrieveChain(address)
	if err != nil {
		return nil, err
	}
	return chain, nil
}

func check(e error) {
	if e != nil {
		log.Print(e.Error())
		return
	}
}

func (h *Handler) getLastBlock(address string) (Block, error) {
	chain, err := h.retrieveChain(address)
	if err != nil {
		return Block{}, err
	}
	length := len(chain)
	lastBlock := chain[length-1]
	return lastBlock, nil
}

func (h *Handler) getWalletMeta(address string) (models.WalletTag, error) {
	var tag models.WalletTag

	path := walletDataPath + address
	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return tag, err
	}
	dataByte, err := h.ReadFile(path + "/data.bin")
	if err != nil {
		return tag, err
	}
	err = gob.NewDecoder(bytes.NewBuffer(dataByte)).Decode(&tag)
	if err != nil {
		return tag, err
	}

	return tag, nil
}

// VerifyChain ... Verify a chain with hash recreation
func (h *Handler) VerifyChain(address string) (bool, error) {
	// this function verifies the last block in the chain by checking the previous one

	chain, err := h.retrieveChain(address)
	if err != nil {
		return false, err
	}

	// if the chain has one block return true
	if len(chain) == 1 {
		block := chain[0]
		stringForHash := "00000000000" + fmt.Sprintf("%f", float64(0)) + address
		shaEngine := sha256.New()
		shaEngine.Write([]byte(stringForHash))

		hashCalc := hex.EncodeToString(shaEngine.Sum(nil))
		if hashCalc == block.Hash {
			return true, nil
		}

		return false, nil
	}

	// get last block
	length := len(chain)
	lastBlock := chain[length-1]

	// get previous block (2nd to last)
	prevBlock := chain[length-2]

	// calculate a hypothetic hash of the last block from prev block data
	stringForHashRec := prevBlock.Hash + fmt.Sprintf("%f", lastBlock.Amount) + address
	shaEngine := sha256.New()
	shaEngine.Write([]byte(stringForHashRec))

	hashCalc := hex.EncodeToString(shaEngine.Sum(nil))

	if hashCalc == lastBlock.Hash {
		return true, nil
	}

	return false, nil
}

// VerifyPrivateKey ... Checks if the hash of the supplied key is equal to the hash of the
// previous blocks key
func (h *Handler) VerifyPrivateKey(address string, privateKey string) (bool, error) {
	chain, err := h.retrieveChain(address)
	if err != nil {
		return false, err
	}

	// get last block
	length := len(chain)
	lastBlock := chain[length-1]

	// hash supplied private key
	stringForHashRec := privateKey
	shaEngine := sha256.New()
	shaEngine.Write([]byte(stringForHashRec))

	hashCalc := hex.EncodeToString(shaEngine.Sum(nil))

	if hashCalc == lastBlock.PrivateKeyHash {
		return true, nil
	}

	return false, nil
}

// BlockExists ...
func (h *Handler) BlockExists(address string, ID string) bool {
	chain, err := h.retrieveChain(address)
	if err != nil {
		h.log.Error("failed to check block exists: %v", err)
		return false
	}

	for x := range chain {
		if ID == chain[x].ID {
			return true
		}
	}

	return false
}
