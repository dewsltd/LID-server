// Package snag provides utility functions for rendering errors to a
// ResponseWriter
package snag

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// E represents an error response to be rendered
type E struct {
	Title  string                 `json:"title"`
	Type   string                 `json:"type"`
	Status int                    `json:"status"`
	Error  string                 `json:"error"`
	Meta   map[string]interface{} `json:"meta,omitempty"`
}

var (
	// ServerError defines a generic server error
	ServerError = E{
		Title:  "Internal Server Error",
		Type:   "server_error",
		Status: http.StatusInternalServerError,
		Error:  "An error occurred while processing this request.",
	}

	// NotFound represents an error shortcut for missing resources
	NotFound = E{
		Title:  "Missing Resource",
		Type:   "not_found",
		Status: http.StatusNotFound,
		Error:  "The requested resource was not found.",
		Meta:   make(map[string]interface{}),
	}

	// SenderWalletNotFound ...
	SenderWalletNotFound = E{
		Title:  "Missing wallet",
		Type:   OpNoSender,
		Status: http.StatusNotFound,
		Error:  "Sender wallet %s not found",
	}

	// RecipientWalletNotFound ...
	RecipientWalletNotFound = E{
		Title:  "Missing wallet",
		Type:   OpNoRecipient,
		Status: http.StatusNotFound,
		Error:  "Reciever wallet %s not found",
	}

	// ChainTransferFailed ...
	ChainTransferFailed = E{
		Title:  "Transaction Failed",
		Type:   TxFailed,
		Status: http.StatusBadRequest,
		Error:  "The transaction failed when submitted to the network: %s",
	}
)

// New records a new snag
func New(title, _type, err string, status int) E {
	return E{
		Title:  title,
		Type:   _type,
		Error:  err,
		Status: status,
	}
}

// Nil checks a snag is empty
func Nil(e E) bool {
	return e.Error == ""
}

// RegisterError recors an error to an E instance
func RegisterError(err error, e *E) {
	e.Error = err.Error()
}

// Render writes a http response to ResponseWriter
// errorBindings holds string bindings for the custom Snags. If errorBindings is
// nil default `Error` field is rendered
func (e E) Render(w http.ResponseWriter, errorBindings ...string) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	if len(errorBindings) > 0 {
		e.Error = fmt.Sprintf(e.Error, errorBindings)
	}

	s, err := json.Marshal(e)
	if err != nil {
		log.Printf("failed to encode Snag: %v", err)
		http.Error(w, "error rendering snag", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(e.Status)
	w.Write(s)
}
