package models

// TransferReq ...
type TransferReq struct {
	SenderPrivateKey        string      `json:"sender_private_key"`
	SenderAddress           string      `json:"sender_address"`
	RecieverAddress         interface{} `json:"reciever_address"`
	RecieverPrivateKey      string      `json:"reciever_private_key"`
	Amount                  string      `json:"amount"`
	SenderBlockID           string      `json:"sender_block_id"`
	RecieverBlockID         string      `json:"reciever_block_id"`
	SenderLastBlockAmount   string      `json:"sender_last_block_amount"`
	RecieverLastBlockAmount string      `json:"reciever_last_block_amount"`
	RewardValidator         string      `json:"reward_validator"`
}
