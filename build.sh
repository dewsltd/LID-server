#!/usr/bin/env bash

echo "Downloading dependencies"
#go mod download

BUILD_DIR=$(pwd)/build
LINUX_EX="QUI-server"

# Build Linux
GOOS=linux go build -ldflags="-s -w" -o "$BUILD_DIR/linux/$LINUX_EX" app.go keys.go
cp .env.example $BUILD_DIR/linux/.env.example
cp server_list.example $BUILD_DIR/linux/server_list.example.json
cp qui-network.service $BUILD_DIR/linux/qui-network.service
